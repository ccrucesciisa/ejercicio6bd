<%-- 
    Document   : index
    Created on : 13-04-2021, 21:25:34
    Author     : ccruces
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.ejercicio6bd.entity.Personas"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Personas> usuarios = (List<Personas>) request.getAttribute("listaPersonas");
    Iterator<Personas> itPersonas = usuarios.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="BaseController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>nombre </th>
             
                    <th> Telefono</th>
                    <th> seleccione</th>
                    </thead>
                    <tbody>
                        <%while (itPersonas.hasNext()) {
                       Personas per = itPersonas.next();%>
                        <tr>
                            <td><%= per.getRut()%></td>
                            <td><%= per.getNombre()%></td>
                          <td><%= per.getTelefono()%></td>
                 <td> <input type="radio" name="seleccion" value="<%= per.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
              <button type="submit" name="accion" value="ver" class="btn btn-success">ver persona</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>

        
        </form>
    </body>
</html>
